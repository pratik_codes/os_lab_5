#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#define  DEFAULT_CHUNK  500  /* 256k */


int copy_file(const char *target, const char *source, const size_t chunk)
{
    const size_t size = (chunk > 0) ? chunk : DEFAULT_CHUNK;
    char        *data, *ptr, *end;
    ssize_t      bytes;
    int          ifd, ofd, err;

    /* NULL and empty file names are invalid. */
    if (!target || !*target || !source || !*source)
        return EINVAL;

    ifd = open(source, O_RDONLY);
    if (ifd == -1)
        return errno;

    /* Create output file; fail if it exists (O_EXCL): */
    ofd = open(target, O_WRONLY | O_CREAT, 0666);
    if (ofd == -1) {
        err = errno;
        close(ifd);
        return err;
    }

    /* Allocate temporary data buffer. */
    data = malloc(size);
    if (!data) {
        close(ifd);
        close(ofd);
        /* Remove output file. */
        unlink(target);
        return ENOMEM;
    }

    /* Copy loop. */
    while (1) {

        /* Read a new chunk. */
        bytes = read(ifd, data, size);
        if (bytes < 0) {
            if (bytes == -1)
                err = errno;
            else
                err = EIO;
            free(data);
            close(ifd);
            close(ofd);
            unlink(target);
            return err;
        } else
        if (bytes == 0)
            break;

        /* Write that same chunk. */
        ptr = data;
        end = data + bytes;
        while (ptr < end) {

            bytes = write(ofd, ptr, (size_t)(end - ptr));
            if (bytes <= 0) {
                if (bytes == -1)
                    err = errno;
                else
                    err = EIO;
                free(data);
                close(ifd);
                close(ofd);
                unlink(target);
                return err;
            } else
                ptr += bytes;
        }
    }

    free(data);

    err = 0;
    if (close(ifd))
        err = EIO;
    if (close(ofd))
        err = EIO;
    if (err) {
        unlink(target);
        return err;
    }
    return 0;
}


void help() {
	
	printf("copy [-m] <file_name> <new_file_name>\n");
	printf("copy [-h] \n");
	return;
	
}


void copy_read_write(char* source, char* destination) { 
	printf("Copying from read/write\n");
	char* source_file;
	char* dest_file;
	source_file = (char*)malloc(sizeof(char)* strlen( source ));
	dest_file   = (char*)malloc(sizeof(char)* strlen( destination ));
	strcpy(source_file, source);
	strcpy(dest_file, destination);	
	copy_file(dest_file,source_file, DEFAULT_CHUNK);
}

void copy_mmap( char* source, char* destination ){
	int sfd, dfd;
  char *src, *dest;
  size_t filesize;
	char* source_file;
	char* dest_file;
	source_file = (char*)malloc(sizeof(char)* strlen( source ));
	dest_file   = (char*)malloc(sizeof(char)* strlen( destination ));
	strcpy(source_file, source);
	strcpy(dest_file, destination);	
	printf("Source: %s\t Dest: %s\t\n", source_file, dest_file); 
 	printf("Copying from memory regions\n");

	/* SOURCE */
  sfd = open(source_file, O_RDONLY);
	if (sfd < -1) {
		printf("File not found!\n");
		exit(1);	
	}
  filesize = lseek(sfd, 0, SEEK_END);
		
  src = mmap(NULL, filesize, PROT_READ, MAP_PRIVATE, sfd, 0);
	if (src < 0) {
		printf("Error using mmap\n");
		exit(1);
	}

  /* DESTINATION */
  dfd = open(dest_file, O_RDWR | O_CREAT, 0666);

  ftruncate(dfd, filesize);

  dest = mmap(NULL, filesize, PROT_READ | PROT_WRITE, MAP_SHARED, dfd, 0);

  /* COPY */
  memcpy(dest, src, filesize);

  if (munmap(src, filesize) == -1)
		printf("Allocation error of source.\n");
  if (munmap(dest, filesize) == -1)
		printf("Allocation error on destination.\n");
	

  close(sfd);
  close(dfd);
}

int main(int argc, char* argv[]) {
	
	int opt;
	if (argc <= 1) { printf("No arguments were provided.\n"); help(); exit(1); }
	while((opt = getopt(argc, argv,"hm:")) != -1) {

		switch(opt) {
			case 'i': printf("I called"); break;
			case 'm': 
							if (argc > 3) {
								copy_mmap(argv[2],argv[3]);	
							}
							else { printf("Arguments not enough!\n"); exit(1); }						
							exit(1);
							break;
			case 'h': 
							
							help();			
							exit(1);							
							break;
			case '?':
							help();
							exit(1);
							break;
		}
	}	
		
	copy_read_write(argv[1], argv[2]);
return 0;
}
